using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using ToolsToLive.AuthCore.Helpers;
using ToolsToLive.AuthCore.IdentityServices;
using ToolsToLive.AuthCore.Interfaces;
using ToolsToLive.AuthCore.Interfaces.Helpers;
using ToolsToLive.AuthCore.Interfaces.IdentityServices;
using ToolsToLive.AuthCore.Interfaces.Model;
using ToolsToLive.AuthCore.Interfaces.Storage;
using ToolsToLive.AuthCore.Model;
using ToolsToLive.AuthCore.OAuth;

namespace ToolsToLive.AuthCore
{
    public static class DependencyInjectionExtensions
    {
        /// <summary>
        /// Sets up dependency for using auth core, including storage, auth options etc.
        /// (storage is setting up as scoped service).
        /// </summary>
        /// <typeparam name="TUser">Type of the user in application.</typeparam>
        /// <typeparam name="TUserStorage">Type of the storrage to set up IUserStorage dependency.</typeparam>
        /// <param name="services">Service collection.</param>
        /// <param name="authOptins">Configuration section that should match to <see cref="AuthOptions"/> class.</param>
        /// <returns></returns>
        public static IServiceCollection AddAuthCore<TUser, TUserStorageService, TRefreshTokenStorageService>(this IServiceCollection services, IConfigurationSection authOptins)
            where TUser : IAuthCoreUser
            where TUserStorageService : class, IUserStorageService<TUser>
            where TRefreshTokenStorageService : class, IRefreshTokenStorageService
        {
            if (string.IsNullOrWhiteSpace(authOptins[nameof(AuthOptions.Key)]))
            {
                throw new ArgumentException("Key must be set in configuration", nameof(authOptins));
            }

            if (string.IsNullOrWhiteSpace(authOptins[nameof(AuthOptions.DeviceIdCookieDomain)]))
            {
                throw new ArgumentException("CookieDomain must be set in configuration", nameof(authOptins));
            }

            services.AddOptions();
            services.Configure<AuthOptions>(authOptins);

            services.AddSingleton<ISigningCredentialsProvider, SigningCredentialsProvider>();
            services.AddSingleton<IIdentityValidationService, IdentityValidationService>();

            services.AddSingleton<IPasswordHasher, PasswordHasher>();
            services.AddSingleton<IIdentityProvider, IdentityProvider>();
            services.AddSingleton<IIdentityService, IdentityService>();
            services.AddSingleton<ICodeGenerator, CodeGenerator>();
            services.AddSingleton<IUserClaimsHelper, UserClaimsHelper>();

            services.AddScoped<IAuthCoreService<TUser>, AuthCoreService<TUser>>();
            services.AddScoped<IUserStorageService<TUser>, TUserStorageService>();
            services.AddScoped<IRefreshTokenStorageService, TRefreshTokenStorageService>();

            services.AddScoped<IAuthCookiesHelper, AuthCookiesHelper>();

            return services;
        }

        public static IServiceCollection AddAuthCoreAuthentication(this IServiceCollection services, IConfigurationSection authOptins)
        {
            var defaults = new AuthOptions();

            var authBuilder = services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    //var paths = new List<string>();

                    //List<X509SecurityKey> signingKeys = paths
                    //    .Where(x => !string.IsNullOrWhiteSpace(x))
                    //    .Select(x => new X509SecurityKey(new System.Security.Cryptography.X509Certificates.X509Certificate2(Path.Combine("appPath", x))))
                    //    .ToList();

                    options.RequireHttpsMetadata = false;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidIssuers = new[] { authOptins[nameof(AuthOptions.Issuer)] ?? defaults.Issuer },
                        ValidateAudience = true,
                        ValidAudiences = new[] { authOptins[nameof(AuthOptions.Audience)] ?? defaults.Audience },
                        ValidateLifetime = true,
                        IssuerSigningKeys = new[] { SigningCredentialsProvider.GetSecurityKey(authOptins[nameof(AuthOptions.Key)]) },
                        ValidateIssuerSigningKey = true,
                        NameClaimType = authOptins[nameof(AuthOptions.UserNameClaimName)] ?? defaults.UserNameClaimName,
                        RoleClaimType = authOptins[nameof(AuthOptions.RoleClaimName)] ?? defaults.RoleClaimName,
                    };
                });

            var oauthOptions = authOptins.GetSection("OAuth");

            authBuilder
                .AddVkontakte(oauthOptions)
                .AddYandex(oauthOptions)
                .AddMailRu(oauthOptions)
                .AddFacebook(oauthOptions);

            return services;
        }

        private static AuthenticationBuilder AddVkontakte(this AuthenticationBuilder authBuilder, IConfigurationSection oauthOptions)
        {
            var config = oauthOptions.GetSection("Vkontakte");

            var vkEnabled = config.GetValue("Enabled", false);
            if (vkEnabled)
            {
                authBuilder.AddVkontakte(options =>
                {
                    options.ClientId = config["ClientId"];
                    options.ClientSecret = config["ClientSecret"];
                    options.CallbackPath = config.GetValue("CallbackPath", "/signin-vkontakte");
                    options.Events.OnTicketReceived = TicketReceived;
                    options.Events.OnAccessDenied = AccessDenied;
                    options.Events.OnRemoteFailure = RemoteFailure;
                    options.ApiVersion = "5.131";
                    options.CorrelationCookie = new CorrelationCookieBuilder(options)
                    {
                        Name = ".AspNetCore.Correlation.",
                        HttpOnly = true,
                        SameSite = SameSiteMode.None,
                        SecurePolicy = CookieSecurePolicy.Always,
                        IsEssential = true,
                    };
                });
            }

            return authBuilder;
        }

        private static AuthenticationBuilder AddYandex(this AuthenticationBuilder authBuilder, IConfigurationSection oauthOptions)
        {
            var config = oauthOptions.GetSection("Yandex");

            var vkEnabled = config.GetValue("Enabled", false);
            if (vkEnabled)
            {
                authBuilder.AddYandex(options =>
                {
                    options.ClientId = config["ClientId"];
                    options.ClientSecret = config["ClientSecret"];
                    options.CallbackPath = config.GetValue("CallbackPath", "/signin-yandex");
                    options.Events.OnTicketReceived = TicketReceived;
                    options.Events.OnAccessDenied = AccessDenied;
                    options.Events.OnRemoteFailure = RemoteFailure;
                    options.CorrelationCookie = new CorrelationCookieBuilder(options)
                    {
                        Name = ".AspNetCore.Correlation.",
                        HttpOnly = true,
                        SameSite = SameSiteMode.None,
                        SecurePolicy = CookieSecurePolicy.Always,
                        IsEssential = true,
                    };
                });
            }

            return authBuilder;
        }

        private static AuthenticationBuilder AddMailRu(this AuthenticationBuilder authBuilder, IConfigurationSection oauthOptions)
        {
            var config = oauthOptions.GetSection("MailRu");

            var vkEnabled = config.GetValue("Enabled", false);
            if (vkEnabled)
            {
                authBuilder.AddMailRu(options =>
                {
                    options.ClientId = config["ClientId"];
                    options.ClientSecret = config["ClientSecret"];
                    options.CallbackPath = config.GetValue("CallbackPath", "/signin-mailru");
                    options.Events.OnTicketReceived = TicketReceived;
                    options.Events.OnAccessDenied = AccessDenied;
                    options.Events.OnRemoteFailure = RemoteFailure;
                    options.CorrelationCookie = new CorrelationCookieBuilder(options)
                    {
                        Name = ".AspNetCore.Correlation.",
                        HttpOnly = true,
                        SameSite = SameSiteMode.None,
                        SecurePolicy = CookieSecurePolicy.Always,
                        IsEssential = true,
                    };
                });
            }

            return authBuilder;
        }

        private static AuthenticationBuilder AddFacebook(this AuthenticationBuilder authBuilder, IConfigurationSection oauthOptions)
        {
            var config = oauthOptions.GetSection("Facebook");

            var vkEnabled = config.GetValue("Enabled", false);
            if (vkEnabled)
            {
                authBuilder.AddFacebook(options =>
                {
                    options.ClientId = config["ClientId"];
                    options.ClientSecret = config["ClientSecret"];
                    options.CallbackPath = config.GetValue("CallbackPath", "/signin-facebook");
                    options.Events.OnTicketReceived = TicketReceived;
                    options.Events.OnAccessDenied = AccessDenied;
                    options.Events.OnRemoteFailure = RemoteFailure;
                    options.CorrelationCookie = new CorrelationCookieBuilder(options)
                    {
                        Name = ".AspNetCore.Correlation.",
                        HttpOnly = true,
                        SameSite = SameSiteMode.None,
                        SecurePolicy = CookieSecurePolicy.Always,
                        IsEssential = true,
                    };
                });
            }

            return authBuilder;
        }

        private class CorrelationCookieBuilder : RequestPathBaseCookieBuilder
        {
            private readonly RemoteAuthenticationOptions _options;

            public CorrelationCookieBuilder(RemoteAuthenticationOptions remoteAuthenticationOptions)
            {
                _options = remoteAuthenticationOptions;
            }

            protected override string AdditionalPath => _options.CallbackPath;

            public override CookieOptions Build(HttpContext context, DateTimeOffset expiresFrom)
            {
                var cookieOptions = base.Build(context, expiresFrom);

                if (!Expiration.HasValue || !cookieOptions.Expires.HasValue)
                {
                    cookieOptions.Expires = expiresFrom.Add(_options.RemoteAuthenticationTimeout);
                }

                return cookieOptions;
            }
        }

        private static Task RemoteFailure(RemoteFailureContext context)
        {
            var logger = context.HttpContext.RequestServices.GetRequiredService<ILogger<AuthOptions>>();
            logger.LogError(context.Failure, "OAuth authorization error");

            var settings = context.HttpContext.RequestServices.GetRequiredService<IOptions<AuthOptions>>();
            context.Response.Redirect(settings.Value.OAuth.OauthLoginPageUrl + "?status=OAuthError");
            context.HandleResponse();
            return Task.CompletedTask;
        }

        private static Task AccessDenied(AccessDeniedContext context)
        {
            var logger = context.HttpContext.RequestServices.GetRequiredService<ILogger<AuthOptions>>();
            logger.LogDebug("OAuth authorization error - access denied");

            var redirUri = context.ReturnUrl;

            if (string.IsNullOrEmpty(redirUri))
            {
                var settings = context.HttpContext.RequestServices.GetRequiredService<IOptions<AuthOptions>>();
                redirUri = settings.Value.OAuth.OauthLoginPageUrl;
            }

            redirUri = GetRedirectUrlWithQueryString(redirUri, "status=OAuthAccessDenied");

            context.Response.Redirect(redirUri);
            context.HandleResponse();
            return Task.CompletedTask;
        }

        private static Task TicketReceived(TicketReceivedContext context)
        {
            var logger = context.HttpContext.RequestServices.GetRequiredService<ILogger<AuthOptions>>();
            logger.LogDebug("OAuth authorization error - TicketReceived");

            if (context.Principal == null)
            {
                logger.LogError("TicketReceived - context.Principal is null");
                return Task.CompletedTask;
            }

            var identityService = context.HttpContext.RequestServices.GetRequiredService<IIdentityService>();
            var codeGenerator = context.HttpContext.RequestServices.GetRequiredService<ICodeGenerator>();
            var authCookiesHelper = context.HttpContext.RequestServices.GetRequiredService<IAuthCookiesHelper>();

            var code = codeGenerator.GenerateCode(12);
            var claims = context.Principal.Claims.ToList();
            claims.Add(new Claim("code", code));
            claims.Add(new Claim("provider", context.Scheme.Name));

            var token = identityService.GenerateJwtToken(claims, TimeSpan.FromMinutes(5));
            authCookiesHelper.SetOauthTokenCookie(context.HttpContext.Response.Cookies, token.Item1, token.Item2);

            var redirUri = context.ReturnUri;
            if (string.IsNullOrEmpty(redirUri))
            {
                var settings = context.HttpContext.RequestServices.GetRequiredService<IOptions<AuthOptions>>();
                redirUri = settings.Value.OAuth.OauthLoginPageUrl;
            }
            redirUri = GetRedirectUrlWithQueryString(redirUri, "code=" + code);

            context.Response.Redirect(redirUri);
            context.HandleResponse();
            return Task.CompletedTask;
        }

        private static string GetRedirectUrlWithQueryString(string baseUrl, string param)
        {
            return baseUrl.Contains("?")
                ? baseUrl + "&" + param
                : baseUrl + "?" + param;
        }
    }
}
