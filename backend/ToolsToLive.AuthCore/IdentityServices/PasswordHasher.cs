using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using Microsoft.Extensions.Options;
using ToolsToLive.AuthCore.Interfaces.IdentityServices;
using ToolsToLive.AuthCore.Model;

namespace ToolsToLive.AuthCore.IdentityServices
{
    public class PasswordHasher : IPasswordHasher
    {
        private readonly IOptions<AuthOptions> _options;

        public PasswordHasher(
            IOptions<AuthOptions> options
            )
        {
            _options = options;
        }

        /// <summary>
        /// Hashes password. It is a good idea to add salt to password.
        /// </summary>
        /// <returns>Hash of password</returns>
        public string HashPassword(string password, string salt = "", int passwordVersion = 1)
        {
            switch (passwordVersion)
            {
                case 1:
                    if (!string.IsNullOrWhiteSpace(_options.Value.ExtraSalt))
                    {
                        password = password + _options.Value.ExtraSalt;
                    }
                    if (!string.IsNullOrWhiteSpace(salt))
                    {
                        password = password + salt;
                    }

                    using (var sha = SHA512.Create())
                    {
                        var hash = sha.ComputeHash(Encoding.Unicode.GetBytes(password));
                        return FromByteToHex(hash);

                    }
                default:
                    throw new NotImplementedException($"Password version {passwordVersion} not supported by PasswordHasher");
            }
        }

        /// <summary>
        /// Verifies password. If you added salt when setting a password, do not forget to add it to the password provided.
        /// </summary>
        /// <returns>True if password matchs to hash</returns>
        public bool VerifyPassword(string hashedPassword, string providedPassword, string salt = "", int passwordVersion = 1)
        {
            if (string.IsNullOrWhiteSpace(hashedPassword))
            {
                return false;
            }
            var hash = HashPassword(providedPassword, salt, passwordVersion).ToLowerInvariant();
            return hash.Equals(hashedPassword.ToLowerInvariant());
        }

        private static string FromByteToHex(byte[] bytes)
        {
            var sb = new StringBuilder();
            foreach (var b in bytes)
            {
                sb.Append(b.ToString("x2", CultureInfo.InvariantCulture).ToUpperInvariant());
            }

            return sb.ToString();
        }
    }
}
