using System.Security.Claims;
using Microsoft.Extensions.Options;
using ToolsToLive.AuthCore.Interfaces.IdentityServices;
using ToolsToLive.AuthCore.Interfaces.Model;
using ToolsToLive.AuthCore.Model;

namespace ToolsToLive.AuthCore.IdentityServices
{
    public class IdentityProvider : IIdentityProvider
    {
        private readonly IOptions<AuthOptions> _authOptions;

        public IdentityProvider(IOptions<AuthOptions> authOptions)
        {
            _authOptions = authOptions;
        }

        //public ClaimsIdentity GetIdentity(IEnumerable<Claim> claims)
        //{
        //    var claimsIdentity = new ClaimsIdentity(
        //        claims,
        //        "Token",
        //        ClaimsIdentity.DefaultNameClaimType,
        //        ClaimsIdentity.DefaultRoleClaimType);

        //    return claimsIdentity;
        //}

        public List<Claim> GetClaimsForUser(IAuthCoreUser user)
        {
            var claims = new List<Claim>
            {
                new Claim(_authOptions.Value.UserNameClaimName, user.UserName),
                new Claim(_authOptions.Value.UserIdClaimName, user.Id),
            };

            if (user.Roles != null)
            {
                foreach (var item in user.Roles)
                {
                    claims.Add(new Claim(_authOptions.Value.RoleClaimName, item));
                }
            }

            if (user.Claims != null && user.Claims.Any())
            {
                claims.AddRange(user.Claims);
            }

            return claims;
        }
    }
}
