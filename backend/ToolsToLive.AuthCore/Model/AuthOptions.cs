using Microsoft.AspNetCore.Http;

namespace ToolsToLive.AuthCore.Model
{
    public class AuthOptions
    {
        /// <summary>
        /// Strong key to sign token. (The longer string is, the stronger key -- 256 chars should be Ok).
        /// Required (should be set in appsettings)
        /// </summary>
        public string Key { get; set; }

        ///// <summary>
        ///// Key string format - is used to convert string to bytes, Unicode by default
        ///// </summary>
        //public KeyEncoding KeyEncoding { get; set; } = KeyEncoding.Unicode;

        /// <summary>
        /// The one that creates the token (identity server).
        /// Any valid string.
        /// </summary>
        public string Issuer { get; set; } = "AuthCoreIssuer";

        /// <summary>
        /// The one for which the token should be created (consumer).
        /// Any valid string.
        /// </summary>
        public string Audience { get; set; } = "AuthCoreAudience";

        /// <summary>
        /// Token lifetime.
        /// </summary>
        public TimeSpan TokenLifetime { get; set; } = TimeSpan.FromMinutes(40);

        /// <summary>
        /// Refresh token lifetime.
        /// </summary>
        public TimeSpan RefreshTokenLifeTime { get; set; } = TimeSpan.FromDays(7);

        /// <summary>
        /// How many attempts can be failed before user is locked (for LockoutPeriod)
        /// </summary>
        public int MaxAccessFailedCount { get; set; } = 8;

        /// <summary>
        /// User lockout period if MaxAccessFailedCount is reached
        /// </summary>
        public TimeSpan LockoutPeriod { get; set; } = TimeSpan.FromSeconds(15);

        /// <summary>
        /// Whether to use the user Id as an additional salt to the password (recommended)
        /// </summary>
        public bool UseUserIdSalt { get; set; } = true;

        /// <summary>
        /// Additional salt that is being added to password
        /// Be careful - if you change it, old passwords will no longer be valid
        /// </summary>
        public string ExtraSalt { get; set; } = "";


        // Cookies

        public bool CookieSecure { get; set; } = true;

        public SameSiteMode CookieSameSiteMode { get; set; } = SameSiteMode.Strict;


        // Auth Cookies (additional token is added to cookies)

        /// <summary>
        /// Wheter cookies is used to stroe token along the way (token will be returned as response to signIn request anyway)
        /// </summary>
        public bool AddTokenToCookie { get; set; } = true;

        /// <summary>
        /// Cookie name
        /// </summary>
        public string AuthCookieName { get; set; } = "AuthCoreCookie";

        /// <summary>
        /// The name of the domain for which the cookie will be set (eg 'mydomain.com')
        /// Required (should be set in appsettings)
        /// </summary>
        public string AuthCookieDomain { get; set; }

        /// <summary>
        /// Path for auth cookie (eg 'api/auth')
        /// '/' by default
        /// </summary>
        public string AuthCookiePath { get; set; } = "/";


        // DeviceId Cookies (additional token is added to cookies)

        /// <summary>
        /// Cookie name
        /// </summary>
        public string DeviceIdCookieName { get; set; } = "AuthCoreDeviceIdCookie";

        /// <summary>
        /// The name of the domain for which the cookie will be set (eg 'mydomain.com')
        /// Required (should be set in appsettings)
        /// </summary>
        public string DeviceIdCookieDomain { get; set; }

        /// <summary>
        /// Path for auth cookie (eg 'api/auth')
        /// '/' by default, but it's strongly recommended to set something like 'api/auth'
        /// </summary>
        public string DeviceIdCookiePath { get; set; } = "/";


        public string TokenTransportClaimName { get; set; } = "AuthCore_TokenTransport";
        public string RoleClaimName { get; set; } = "AuthCore_Role";
        public string UserIdClaimName { get; set; } = "AuthCore_UserId";
        public string UserNameClaimName { get; set; } = "AuthCore_UserName";

        public OAuthOptions OAuth { get; set; }

        //public TimeSpan EmailConfirmCodeLifeTime { get; set; }

        //public TimeSpan ChangePasswordCodeLifeTime { get; set; }

        //public string OauthRedirectUrl { get; set; }
    }

    public class OAuthOptions
    {
        public string OauthLoginPageUrl { get; set; }

        public bool OAuthHttpsScheme { get; set; } = true;
    }

    //public enum KeyEncoding
    //{
    //    Unicode,
    //    Utf8
    //}
}
