namespace ToolsToLive.AuthCore.Model
{
    public class RefreshToken
    {
        public string UserId { get; set; }
        public string Token { get; set; }
        public DateTime IssueDate { get; set; }
        public DateTime ExpireDate { get; set; }

        public string PreviousToken { get; set; }
    }
}
