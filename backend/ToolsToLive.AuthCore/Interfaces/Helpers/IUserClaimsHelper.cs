using System.Security.Claims;

namespace ToolsToLive.AuthCore.Interfaces.Helpers
{
    public interface IUserClaimsHelper
    {
        string? GetUserId(ClaimsPrincipal user);
        string? GetUserName(ClaimsPrincipal user);
        string? GetTokenTransport(ClaimsPrincipal user);
        string[] RolesUserName(ClaimsPrincipal user);
    }
}
