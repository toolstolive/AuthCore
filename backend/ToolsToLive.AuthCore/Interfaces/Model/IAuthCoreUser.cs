using System.Security.Claims;

namespace ToolsToLive.AuthCore.Interfaces.Model
{
    public interface IAuthCoreUser
    {
        /// <summary>
        /// User identifier (will be added to token)
        /// </summary>
        string Id { get; }

        /// <summary>
        /// User name (will be added to token)
        /// </summary>
        string UserName { get; }

        /// <summary>
        /// Password hash (from/to database) (is used to save passwordHash to storage)
        /// </summary>
        string PasswordHash { get; set; }

        /// <summary>
        /// User's roles (will be added to token)
        /// </summary>
        List<string> Roles { get; }

        /// <summary>
        /// Additional claims (will be added to token)
        /// </summary>
        List<Claim> Claims { get; }

        DateTime? LockoutEndDate { get; set; }

        int AccessFailedCount { get; set; }

        /// <summary>
        /// Version of the password -- can be used if you changed salt or old users with old password hashes exist in database
        /// </summary>
        int PasswordVersion { get; set; }
    }
}
