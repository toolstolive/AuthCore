using System.Security.Claims;
using Microsoft.Extensions.Options;
using ToolsToLive.AuthCore.Interfaces.Helpers;
using ToolsToLive.AuthCore.Model;

namespace ToolsToLive.AuthCore.Helpers
{
    public class UserClaimsHelper : IUserClaimsHelper
    {
        private readonly IOptions<AuthOptions> _authOptions;

        public UserClaimsHelper(IOptions<AuthOptions> authOptions)
        {
            _authOptions = authOptions;
        }

        public string? GetUserId(ClaimsPrincipal user)
        {
            return user.GetClaimByClaimType(_authOptions.Value.UserIdClaimName);
        }

        public string? GetUserName(ClaimsPrincipal user)
        {
            return user.GetClaimByClaimType(_authOptions.Value.UserNameClaimName);
        }

        public string? GetTokenTransport(ClaimsPrincipal user)
        {
            return user.GetClaimByClaimType(_authOptions.Value.TokenTransportClaimName);
        }

        public string[] RolesUserName(ClaimsPrincipal user)
        {
            return user.GetClaimsByClaimType(_authOptions.Value.RoleClaimName);
        }
    }

    public static class UserClaimsHelperStatic
    {
        public static string? GetClaimByClaimType(this ClaimsPrincipal user, string claimType)
        {
            if (user == null || user.Claims == null)
            {
                return null;
            }
            return user.Claims.FirstOrDefault(x => x.Type == claimType)?.Value;
        }

        public static string[] GetClaimsByClaimType(this ClaimsPrincipal user, string claimType)
        {
            if (user == null || user.Claims == null)
            {
                return Array.Empty<string>();
            }
            return user.Claims.Where(x => x.Type == claimType).Select(x => x.Value).ToArray();
        }
    }
}
