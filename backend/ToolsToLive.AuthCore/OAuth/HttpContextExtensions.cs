using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace ToolsToLive.AuthCore.OAuth
{
    public static class HttpContextExtensions
    {
        public static async Task<AuthenticationScheme[]> GetExternalProvidersAsync(this HttpContext context)
        {
            var schemes = context.RequestServices.GetRequiredService<IAuthenticationSchemeProvider>();

            var result = (from scheme in await schemes.GetAllSchemesAsync()
                          where !string.IsNullOrEmpty(scheme.DisplayName)
                          select scheme).ToArray();
            return result;
        }

        public static async Task<bool> IsProviderSupportedAsync(this HttpContext context, string provider)
        {
            return (from scheme in await context.GetExternalProvidersAsync()
                    where string.Equals(scheme.Name, provider, StringComparison.OrdinalIgnoreCase)
                    select scheme).Any();
        }
    }
}
