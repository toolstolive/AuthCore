
export interface RefreshToken {
  userId: string;
  token: string;
  issueDate: Date;
  expireDate: Date;

  // Date of token expiration in current browser (just in case if browser time is different from server)
  browserExpireDate: Date;
  lifeTime: number;
}
