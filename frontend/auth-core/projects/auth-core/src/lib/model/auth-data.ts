import { ClaimModel } from './claim-model';

export interface AuthData<TUser> {
    user: TUser;
    accessToken: string;

    browserExpireDate: number; // Date.valueOf()
    lifeTime: number;
    claims: ClaimModel[];

    /** (internal needs) is used to check if refreshToken issued for current user */
    refreshTokenUserId: string;
}
