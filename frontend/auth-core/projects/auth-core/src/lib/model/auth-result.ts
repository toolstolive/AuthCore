import { AuthResultType } from './auth-result-type';
import { RefreshToken } from './refresh-token';

export interface AuthResult<TUser> {
  result: AuthResultType;
  user: TUser | null;
  accessToken: string | null;
  refreshToken: RefreshToken | null;
  isSuccess: boolean;
}
