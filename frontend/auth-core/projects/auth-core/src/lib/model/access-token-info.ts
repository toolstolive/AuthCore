import { ClaimModel } from './claim-model';

export interface AccessTokenInfo {
  /**
   * Issue date (seconds past 1970-01-01 00:00:00Z)
   */
  issued: number; // set by server (according to date and time on the server)

  /**
   * Expiration date (seconds past 1970-01-01 00:00:00Z)
   */
  expires: number; // set by server (according to date and time on the server)

  /**
   * All other claims
   */
  claims: ClaimModel[];
}
