import { ActivatedRouteSnapshot, CanActivate, CanLoad, Data, Route, Router, RouterStateSnapshot, UrlSegment, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { AuthData } from '../model/auth-data';
import { AccessService } from '../services/access.service';
import { AuthCoreSettings, AUTH_CORE_SETTINGS_TOKEN } from '../model/auth-core-settings';

/**
 * Without any parameters guards checks if user is authenticated
 * With route.data['roles'] guard checks if users is authenticated and has at least one of the roles
 * route.data['roles'] - array of roles that allow access to this resource (user must have at least one of these roles)
 * route.data['navigateToWhenNotAuth'] - array of commands and a starting point that will be passed to router.navigate() if user not authenticated. This parameter overrides corresponding global setting.
 * route.data['navigateToWhenNotInRole'] - array of commands and a starting point that will be passed to router.navigate() if user not in a role (based on route.data['roles']). This parameter overrides corresponding global setting.
 */
@Injectable({
  providedIn: 'root'
})
export class AuthCoreGuard<TUser> implements CanLoad, CanActivate {

  constructor(private authService: AuthService<TUser>,
              private accessService: AccessService<TUser>,
              @Inject(PLATFORM_ID) private platformId: any,
              @Inject(AUTH_CORE_SETTINGS_TOKEN) private settings: AuthCoreSettings,
              private router: Router) {
  }

  canLoad(
    route: Route,
    segments: UrlSegment[]): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
      return this.checkAccess(route.data);
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      return this.checkAccess(route.data);
  }

  private checkAccess(routeData: Data | undefined): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.authService.getAuthDataAsync()
        .then((authData: AuthData<TUser> | null) => {
          if (authData === null) {
            this.redirectNotAuth(routeData);
            resolve(false);
          }
          if (!this.checkRoles(routeData, authData)) {
            this.redirectNotInRole(routeData);
            resolve(false);
          }

          resolve(true);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  private checkRoles(data: Data | undefined, authData: AuthData<TUser> | null): boolean {
    if (!data) {
      return true;
    }
    const roles = data['roles'] as Array<string>;

    if (roles && roles.length > 0) {
      return this.accessService.isInOneOfRoles(authData, ...roles);
    }
    return true;
  }

  private redirectNotAuth(routeData: Data | undefined) {
    if (routeData && routeData['navigateToWhenNotAuth']) {
      this.router.navigate(routeData['navigateToWhenNotAuth']);
    } else if (this.settings.navigateToWhenNotAuth) {
      this.router.navigate(this.settings.navigateToWhenNotAuth);
    }
  }

  private redirectNotInRole(routeData: Data | undefined) {
    if (routeData && routeData['navigateToWhenNotInRole']) {
      this.router.navigate(routeData['navigateToWhenNotInRole']);
    } else if (this.settings.navigateToWhenNotInRole) {
      this.router.navigate(this.settings.navigateToWhenNotInRole);
    }
  }
}
