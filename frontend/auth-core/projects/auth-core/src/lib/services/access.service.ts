import { Inject, Injectable } from '@angular/core';
import { AuthCoreSettings, AUTH_CORE_SETTINGS_TOKEN } from '../model/auth-core-settings';
import { AuthData } from '../model/auth-data';
import { ClaimModel } from '../model/claim-model';
import { AuthService } from './auth.service';

@Injectable()
export class AccessService<TUser> {

  // public static readonly TokenTransportClaim = 'AuthCoreTokenTransport';
  // public static readonly RoleClaim = 'http://schemas.microsoft.com/ws/2008/06/identity/claims/role';
  // public static readonly UserIdClaim = 'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier';
  // public static readonly UserNameClaim = 'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name';

  constructor(
    @Inject(AUTH_CORE_SETTINGS_TOKEN) private settings: AuthCoreSettings,
    private authService: AuthService<TUser>
  ) { }

  async getRolesAsync(): Promise<string[]> {
    const authData = await this.authService.getAuthDataAsync();
    return this.getRoles(authData);
  }

  getRoles(authData: AuthData<TUser> | null): string[] {
    const roles = new Array<string>();
    if (!authData || !authData.claims || authData.claims.length === 0) {
      return roles;
    }
    const claims = this.getClaims(authData, this.settings.roleClaimName ?? 'AuthCore_Role');
    claims.forEach(claim => {
      roles.push(claim.value);
    });
    return roles;
  }

  async isInOneOfRolesAsync(...role: string[]): Promise<boolean> {
    const authData = await this.authService.getAuthDataAsync();
    return this.isInOneOfRoles(authData, ...role);
  }

  isInOneOfRoles(authData: AuthData<TUser> | null, ...roles: string[]): boolean {
    if (!roles || !roles.length) {
      return false;
    }
    const res = roles.findIndex(x => this.isInRole(authData, x)) > -1;
    return res;
  }

  async isInRoleAsync(role: string): Promise<boolean> {
    const authData = await this.authService.getAuthDataAsync();
    return this.isInRole(authData, role);
  }

  isInRole(authData: AuthData<TUser> | null, role: string): boolean {
    if (!authData?.claims) {
      return false;
    }
    return this.hasClaim(authData, this.settings.roleClaimName ?? 'AuthCore_Role', role);
  }

  async getClaimsAsync(type: string): Promise<ClaimModel[]> {
    const authData = await this.authService.getAuthDataAsync();
    return this.getClaims(authData, type);
  }

  getClaims(authData: AuthData<TUser> | null, type: string): ClaimModel[] {
    if (!authData?.claims || !(authData?.claims.length > 0)) {
      return new Array<ClaimModel>();
    }

    return authData.claims.filter(x => x.type === type);
  }

  async hasAnyClaimAsync(type: string): Promise<boolean> {
    const authData = await this.authService.getAuthDataAsync();
    return this.hasAnyClaim(authData, type);
  }

  hasAnyClaim(authData: AuthData<TUser> | null, type: string): boolean {
    if (!authData?.claims || !(authData?.claims.length > 0)) {
      return false;
    }
    if (authData.claims.findIndex(x => x.type === type) > -1) {
      return true;
    }
    return false;
  }

  async hasClaimAsync(type: string, value: string): Promise<boolean> {
    const authData = await this.authService.getAuthDataAsync();
    return this.hasClaim(authData, type, value);
  }

  hasClaim(authData: AuthData<TUser> | null, type: string, value: string): boolean {
    if (!authData?.claims || !(authData?.claims.length > 0)) {
      return false;
    }
    if (authData.claims.findIndex(x => x.type === type && x.value === value) > -1) {
      return true;
    }
    return false;
  }
}
