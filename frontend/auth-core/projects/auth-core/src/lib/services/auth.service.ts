import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { AuthData } from '../model/auth-data';
import { AuthResult } from '../model/auth-result';
import { AuthDataService } from './internal/auth-data.service';
import { DeviceIdService } from './internal/device-id.service';
import { SignInService } from './internal/sign-in.service';
import { TokenRefreshService } from './internal/token-refresh.service';

@Injectable()
export class AuthService<TUser> {
  private currentAuthDataSubject = new BehaviorSubject<AuthData<TUser> | null>(null);

  public readonly currentAuthData$ = this.currentAuthDataSubject.asObservable();
  public readonly currentUser$ = this.currentAuthDataSubject.pipe(
    map(authData => authData?.user ?? null)
  );

  private currentAuthData: AuthData<TUser> | null;

  constructor(
    private deviceIdService: DeviceIdService,
    private signInService: SignInService<TUser>,
    private authDataService: AuthDataService<TUser>,
    private tokenRefreshService: TokenRefreshService<TUser>,
    ) {
    this.currentAuthData = null;
  }

  async getUser(): Promise<TUser | null> {
    const authData = await this.getAuthDataAsync();
    return authData?.user ?? null;
  }

  getAuthDataAsync(): Promise<AuthData<TUser> | null> {
    const authDataPromise = this.authDataService.getAuthDataAsync();
    authDataPromise.then((authData: AuthData<TUser> | null) => {
      if (!this.compareAuthData(this.currentAuthData, authData)) {
        this.authUpdate(authData);
      }
    }).catch(err => {});
    return authDataPromise;
  }

  /**
   * SignIn
   * @param userName  - user name (login)
   * @param password - password (plain text)
   * @returns 'AuthData<TUser>' if successed (resolved) or 'AuthResult<TUser> | null' if not (rejected)
   */
  signInAsync(userName: string, password: string): Promise<AuthData<TUser> | null> {
    const signInResult = this.signInService.signInAsync(userName, password);
    signInResult.then((authData: AuthData<TUser>) => {
      this.authUpdate(authData);
    }).catch((err: AuthResult<TUser> | null) => {
      this.authUpdate(null);
    });
    return signInResult;
  }

  /**
   * Manual token refresh -- can be useful when user's claims should be changed or smth like this
   * @returns AuthData<TUser>
   */
  async refreshTokenAsync(): Promise<AuthData<TUser> | null> {
    try {
      const authData = await this.tokenRefreshService.refreshTokenAsync();
      this.authUpdate(authData);
      return authData;
    } catch (error) {
      this.authUpdate(null);
      return null;
    }
  }

  /**
   * Can be used if AuthData is obtained in other ways - for example, when registering, verifying email, etc.
   * @param authResult 
   * @returns 'AuthData<TUser>' if successed (resolved) or 'AuthResult<TUser> | null' if not (rejected)
   */
  customSignIn(authResult: AuthResult<TUser>): AuthData<TUser> | null {
    const result = this.signInService.customsignIn(authResult);
    this.authUpdate(result);
    return result;
  }

  /**
   * ExterhalSignIn. In fact user will be redirected to OAuth flow
   * @param provider  - OAuth provider
   * @param params - query string params that will be added to request
   */
  externalSignIn(provider: string, params?: Map<string, string>) {
    this.signInService.externalSignIn(provider, params);
  }

  signOutAsync(): Promise<void> {
    const signInResult = this.signInService.signOutAsync();
    signInResult.then(() => {
      this.authUpdate(null);
    }).catch(() => {
      this.authUpdate(null);
    });
    return signInResult;
  }

  createDeviceId(): string {
    return this.deviceIdService.createDeviceId();
  }

  private authUpdate(authData: AuthData<TUser> | null): void {
    this.currentAuthData = authData;
    this.currentAuthDataSubject.next(authData);
  }

  /**
   * If equal - returns true
   */
  private compareAuthData(a1: AuthData<TUser> | null, a2: AuthData<TUser> | null): boolean {
    if (a1 === null && a2 === null) {
      return true;
    }

    if (a1 === null || a2 === null) {
      return false;
    }

    if (a1.claims?.length !== a2.claims?.length) {
      return false;
    }

    if (a1.claims && a1.claims.length > 0 && a2.claims && a2.claims.length > 0) {
      let isDifferent = false;
      for (let index = 0; index < a1.claims.length; index++) {
        if (a1.claims[index] !== a2.claims[index]) {
          isDifferent = true;
        }
      }
      if (isDifferent) {
        return false;
      }
    }

    // if (a1.Token.UserId === a2.Token.UserId &&
    //     a1.Token.Token === a2.Token.Token &&
    //     a1.User.Id === a2.User.Id &&
    //     a1.User.Roles === a2.User.Roles &&
    //     a1.User.Roles.length === a2.User.Roles.length) {
    //     return true;
    //   }

    return true;
  }
}
