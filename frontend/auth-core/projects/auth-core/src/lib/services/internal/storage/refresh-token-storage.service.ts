import { isPlatformBrowser } from '@angular/common';
import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { AuthCoreSettings, AUTH_CORE_SETTINGS_TOKEN } from '../../../model/auth-core-settings';
import { RefreshToken } from '../../../model/refresh-token';
import { CookieStorageService } from './cookie-storage.service';
import { LocalStorageService } from './local-storage.service';

@Injectable()
export class RefreshTokenStorage {
  private readonly storageKey: string = 'mydomain_rt';

  constructor(
    @Inject(PLATFORM_ID) private platformId: any,
    @Inject(AUTH_CORE_SETTINGS_TOKEN) private settings: AuthCoreSettings,
    private localStorage: LocalStorageService,
    private cookieStorage: CookieStorageService,
  ) {
    this.storageKey = this.settings.refreshTokenCookieDomain + '_rt';
  }

  load(): RefreshToken | null {
    if (!isPlatformBrowser(this.platformId)) {
      return null;
    }
    
    let fromStore: string | null;

    switch (this.settings.refreshTokenStorage ?? 'localStorage') {
      case 'cookie':
        fromStore = this.cookieStorage.load(this.storageKey);
        break;
      // case 'sessionStorage': // sessionStorage can be used if user set checkbox "do not remember me"
      //   try {
      //     fromStore = this.sessionStorage.load(this.storageKey);
      //   } catch (error) {
      //     console.warn('Unable to use sessionStorage, fallback to cookie storage');
      //     fromStore = this.cookieStorage.load(this.storageKey); // fallback to cookie storage
      //   }
      //   break;
      case 'localStorage':
        try {
          fromStore = this.localStorage.load(this.storageKey);
        } catch (error) {
          console.warn('Unable to use localStorage, fallback to cookie storage');
          fromStore = this.cookieStorage.load(this.storageKey); // fallback to cookie storage
        }
        break;
      default:
        throw new Error('Unexpected value of refreshTokenStorage parameter');
    }

    if (!fromStore) {
      return null;
    }

    try {
      const result: RefreshToken = JSON.parse(fromStore);

      result.issueDate = new Date(result.issueDate);
      result.expireDate = new Date(result.expireDate);
      result.browserExpireDate = new Date(result.browserExpireDate);
      return result;

    } catch (error) {
      console.error(error);
      return null;
    }
  }

  isExist(): boolean {
    if (!isPlatformBrowser(this.platformId)) {
      return false;
    }

    switch (this.settings.refreshTokenStorage ?? 'localStorage') {
      case 'cookie':
        return this.cookieStorage.isExist(this.storageKey);

      case 'localStorage':
        try {
          return this.localStorage.isExist(this.storageKey);
        } catch (error) {
          console.warn('Unable to use localStorage, fallback to cookie storage');
          return this.cookieStorage.isExist(this.storageKey); // fallback to cookie storage
        }
      default:
        throw new Error('Unexpected value of refreshTokenStorage parameter');
    }
  }

  save(refreshToken: RefreshToken): void {
    if (!isPlatformBrowser(this.platformId)) {
      return;
    }

    const realDiff = refreshToken.expireDate.valueOf() - refreshToken.issueDate.valueOf();
    const expDateValue = (new Date()).valueOf() + realDiff - 3000;
    refreshToken.browserExpireDate = new Date(expDateValue); // time in the browser may differ from the time on the server
    refreshToken.lifeTime = realDiff;
    const toStore = JSON.stringify(refreshToken);

    if (
      !this.settings.refreshTokenCookieDomain ||
      typeof this.settings.refreshTokenCookieDomain !== 'string' ||
      this.settings.refreshTokenCookieDomain.length < 2
    ) {
      console.error(
        'Setting refreshTokenCookieDomain is not set. Unable to store refresh token.'
      );
    }

    switch (this.settings.refreshTokenStorage ?? 'localStorage') {
      case 'cookie':
        this.cookieStorage.save(this.storageKey, toStore, 
          {
            expires: refreshToken.browserExpireDate,
            httpOnly: false,
            path: this.settings.refreshTokenCookiePath,
            sameSite: this.settings.refreshTokenCookieSameSite,
            secure: this.settings.refreshTokenCookieSecure,
            domain: this.settings.refreshTokenCookieDomain,
          });
        break;
      case 'localStorage':
        try {
          this.localStorage.save(this.storageKey, toStore);
        } catch (error) {
          console.warn('Unable to use localStorage, fallback to cookie storage');
          this.cookieStorage.save(this.storageKey, toStore, 
            {
              expires: refreshToken.browserExpireDate,
              httpOnly: false,
              path: this.settings.refreshTokenCookiePath,
              sameSite: this.settings.refreshTokenCookieSameSite,
              secure: this.settings.refreshTokenCookieSecure,
              domain: this.settings.refreshTokenCookieDomain,
            }); // fallback to cookie storage
        }
        break;
      default:
        throw new Error('Unexpected value of refreshTokenStorage parameter');
    }
  }

  clean(): void {
    if (!isPlatformBrowser(this.platformId)) {
      return;
    }

    switch (this.settings.refreshTokenStorage ?? 'localStorage') {
      case 'cookie':
        this.cookieStorage.clean(this.storageKey, {
          httpOnly: false,
          path: this.settings.refreshTokenCookiePath,
          sameSite: this.settings.refreshTokenCookieSameSite,
          secure: this.settings.refreshTokenCookieSecure,
          domain: this.settings.refreshTokenCookieDomain,
        });;
        break;
      case 'localStorage':
        try {
          this.localStorage.clean(this.storageKey);
        } catch (error) {
          console.warn('Unable to use localStorage, fallback to cookie storage');
          this.cookieStorage.clean(this.storageKey, {
            httpOnly: false,
            path: this.settings.refreshTokenCookiePath,
            sameSite: this.settings.refreshTokenCookieSameSite,
            secure: this.settings.refreshTokenCookieSecure,
            domain: this.settings.refreshTokenCookieDomain,
          }); // fallback to cookie storage
        }
        break;
      default:
        throw new Error('Unexpected value of refreshTokenStorage parameter');
    }
  }
}
