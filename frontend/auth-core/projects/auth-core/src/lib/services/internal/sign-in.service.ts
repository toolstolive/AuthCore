import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { AuthData } from '../../model/auth-data';
import { AuthResult } from '../../model/auth-result';
import { AccessTokenStorage } from './storage/access-token-storage.service';
import { AuthApiService } from './auth-api.service';
import { DeviceIdService } from './device-id.service';
import { RefreshTokenStorage } from './storage/refresh-token-storage.service';
import { AuthResultType } from '../../model/auth-result-type';
import { AuthCoreSettings, AUTH_CORE_SETTINGS_TOKEN } from '../../model/auth-core-settings';
import { isPlatformBrowser } from '@angular/common';

@Injectable()
export class SignInService<TUser> {

constructor(
  private deviceIdService: DeviceIdService,
  private authApiService: AuthApiService<TUser>,
  private accessTokenStorage: AccessTokenStorage<TUser>,
  private refreshTokenStorage: RefreshTokenStorage,
  @Inject(PLATFORM_ID) private platformId: any,
  @Inject(AUTH_CORE_SETTINGS_TOKEN) private settings: AuthCoreSettings,
) { }

/**
 * sign in
 * @param userName - user name (login)
 * @param password - password
 * @returns 'AuthData<TUser>' if successed or 'AuthResult<TUser> | null' if not
 */
  signInAsync(userName: string, password: string): Promise<AuthData<TUser>> {
    const result = new Promise<AuthData<TUser>>((resolve, reject) => {
      const deviceId = this.deviceIdService.createDeviceId();

      const authDataRequest = this.authApiService.signInAsync(userName, password, deviceId);
      authDataRequest.then((data: AuthResult<TUser> | null) => {
        if (data?.isSuccess && data.refreshToken && data.accessToken && data.user) {
          const authData = this.accessTokenStorage.save(data);
          this.refreshTokenStorage.save(data.refreshToken);
          resolve(authData);
        } else {
          reject(data);
        }
      }).catch((error: any) => {
          console.error(error);
          const authres: AuthResult<TUser> = {
            isSuccess: false,
            result: AuthResultType.Failed,
            refreshToken: null,
            accessToken: null,
            user: null,
          };
          reject(authres);
      });
    });
    return result;
  }

  customsignIn(authResult: AuthResult<TUser>): AuthData<TUser> | null {
    if (authResult?.isSuccess && authResult.refreshToken && authResult.accessToken && authResult.user) {
      const authData = this.accessTokenStorage.save(authResult);
      this.refreshTokenStorage.save(authResult.refreshToken);
      return authData;
    }
    return null;
  }

  externalSignIn(provider: string, params?: Map<string, string>) {
    if (!isPlatformBrowser(this.platformId)) {
      console.warn('User can not be authorized by OAuth on server side');
      return;
    }
    
    const path = this.settings.externalSignInPath ?? 'ExternalSignIn';
    let url = `${this.settings.identityServerUrl}/${path}?provider=${provider}`;
    if (params && params.size > 0) {
      for (const param of params) {
        if (param[0] && param[1]) {
          url += `&${param[0]}=${param[1]}`;
        }
      }
    }

    window.location.href = url;
  }

  signOutAsync(): Promise<void> {
    const result = new Promise<void>((resolve, reject) => {
      this.authApiService.signOutAsync()
        .then(() => {
        }).catch((err) => {
          console.log(err);
          console.warn('Something went wrong - unable to sign out');
        }).finally(() => {
          this.accessTokenStorage.clean();
          this.refreshTokenStorage.clean();
          resolve();
        });
    });
    return result;
  }

  signOutFromEverywhereAsync(): Promise<void> {
    const result = new Promise<void>((resolve, reject) => {
      this.authApiService.signOutFromEverywhereAsync()
        .then(() => {
        }).catch((err) => {
          console.log(err);
          console.warn('Something went wrong - unable to sign out');
        }).finally(() => {
          this.accessTokenStorage.clean();
          this.refreshTokenStorage.clean();
          resolve();
        });
    });
    return result;
  }
}
