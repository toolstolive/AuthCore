import { isPlatformServer } from '@angular/common';
import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { AuthData } from '../../model/auth-data';
import { AccessTokenStorage } from './storage/access-token-storage.service';
import { RefreshTokenStorage } from './storage/refresh-token-storage.service';
import { TokenRefreshService } from './token-refresh.service';

@Injectable()
export class AuthDataService<TUser> {

  private resultPromise?: Promise<AuthData<TUser> | null>;

  constructor(
    @Inject(PLATFORM_ID) private platformId: any,
    private accessTokenStorage: AccessTokenStorage<TUser>,
    private tokenRefreshService: TokenRefreshService<TUser>,
    private refreshTokenStorage: RefreshTokenStorage,
    ) { }

  getAuthDataAsync(): Promise<AuthData<TUser> | null> {
    // Call the logic and api only once, and if someone else wants to get data -- return promise
    if (this.resultPromise) {
      return this.resultPromise;
    }

    const resultPromise = new Promise<AuthData<TUser> | null>((resolve, reject) => {
      // On the server side user is never authenticated!
      if (isPlatformServer(this.platformId)) {
        resolve(null);
      }

      const rt = this.refreshTokenStorage.load();
      if (!rt) {
        this.accessTokenStorage.clean();
        resolve(null);
        return;
      }

      const authData = this.accessTokenStorage.load();
      
      // if there is no access token, but there is a refresh token we need to get new access token by refreshing it.
      // if user was changed on different site sessionStorage can still keep old accessToken. We need to refresh it.
      if (!authData || rt.userId !== authData.refreshTokenUserId) {
        this.tokenRefreshService.refreshTokenAsync()
          .then((data: AuthData<TUser>) => {
            resolve(data);
          }).catch((error: any) => {
            resolve(null);
          }).finally(() => {
          });
        return;
      }

      // const realDiff = authData.Token.ExpireDate.valueOf() - authData.Token.IssueDate.valueOf();
      // const timePass = ((new Date()).valueOf() - authData.Token.IssuedBrouserDate.valueOf()) + 5000;
      const now = (new Date()).valueOf();
      const exp = authData.browserExpireDate.valueOf();

      // Если срок жизни токена истёк
      if (now > exp) {
        this.tokenRefreshService.refreshTokenAsync()
          .then((data: AuthData<TUser>) => {
            resolve(data);
          }).catch((error: any) => {
            resolve(null);
          }).finally(() => {
          });
        return;
      } else {
        // Refresh token in the background if 3 quarters of the token lifetime  has passed
        if ((exp - now) < authData.lifeTime / 4) {
          this.tokenRefreshService.refreshTokenAsync();
        }
      }

      resolve(authData);
    });

    this.resultPromise = resultPromise;
    resultPromise.finally(() => {
      this.resultPromise = undefined;
    });

    return resultPromise;
  }
}
