import { Injectable } from '@angular/core';
import { AuthData } from '../../model/auth-data';
import { AuthResult } from '../../model/auth-result';
import { AccessTokenStorage } from './storage/access-token-storage.service';
import { AuthApiService } from './auth-api.service';
import { RefreshTokenStorage } from './storage/refresh-token-storage.service';

@Injectable()
export class TokenRefreshService<TUser> {

  private resultPromise?: Promise<AuthData<TUser>>;

  constructor(
    private authApiService: AuthApiService<TUser>,
    private accessTokenStorage: AccessTokenStorage<TUser>,
    private refreshTokenStorage: RefreshTokenStorage,
    ) { }

  refreshTokenAsync(): Promise<AuthData<TUser>> {
    // Call the logic and api only once, and if someone else wants to get data -- return promise
    if (this.resultPromise) {
      return this.resultPromise;
    }

    const resultPromise = new Promise<AuthData<TUser>>((resolve, reject) => {
      const refreshToken = this.refreshTokenStorage.load();
      if (!refreshToken){
        reject();
        return;
      }

      const authDataRequest = this.authApiService.refreshTokenAsync(refreshToken.userId, refreshToken.token, '');
      authDataRequest.then((data: AuthResult<TUser> | null) => {
        if (data?.isSuccess && data.refreshToken) {
          const authData = this.accessTokenStorage.save(data);
          this.refreshTokenStorage.save(data.refreshToken);
          resolve(authData);
        } else {
          this.refreshTokenStorage.clean();
          this.accessTokenStorage.clean();
          console.warn('Something went wrong - unable to refresh token');
          reject('Something went wrong - unable to refresh token');
        }
      }).catch((error: any) => {
        console.error(error);
        reject(error);
      }).finally(() => {
      });
    });

    this.resultPromise = resultPromise;
    resultPromise.finally(() => {
      this.resultPromise = undefined;
    });

    return this.resultPromise;
  }
}
