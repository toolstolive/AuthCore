import { Injectable } from '@angular/core';
import { AccessTokenInfo } from '../../model/access-token-info';
import { ClaimModel } from '../../model/claim-model';
import { CryptoService } from './storage/crypto.service';

@Injectable()
export class TokenParserService {

  constructor(
    private crypto: CryptoService,
  ) { }

  parseToken(token: string): AccessTokenInfo {
    const start = token.indexOf('.') + 1;
    const end = token.indexOf('.', start);
    const payload = token.substring(start, end);
    const pl =  this.crypto.decode(payload);
    if (!pl) {
      throw new Error('Unable to parse access token');
    }
    const plObj = JSON.parse(pl);

    const claims = new Array<ClaimModel>();
    for (const key in plObj) {
      if (Object.prototype.hasOwnProperty.call(plObj, key)) {
        const element = plObj[key];
        if (key === 'exp' || key === 'nbf') {
          continue;
        }
        if (typeof element === 'string') {
          claims.push({
            type: key,
            value: element,
          });
        } else {
          if (typeof element === 'object' && element.length !== undefined) {
            element.forEach((claimItem: string) => {
              claims.push({
                type: key,
                value: claimItem,
              });
            });
          }
        }
      }
    }

    const info: AccessTokenInfo = {
      expires: plObj.exp,
      issued: plObj.nbf, // plObj.iat // Issued at
      claims: claims
    };
    return info;
  }
}
